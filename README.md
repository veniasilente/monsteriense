# monsteriense

Dictionary files for Monster Hunter-related stuff, covering wyvern species names, item and weapon names, location names up to at least G5 (World). 

Compatible with eg.: LibreOffice. 

## Contents

Dictionary files are organized in folders representing categories: monsters (the most common), materials, locations.

Monster files are organized according to the naming schema of the form ``monhun_{taxonomy}_{lang}.dic``.

Material and Location names are organized in one file each, of the form ``monhun_{category}_{lang}.dic``.

* ``{category}`` describes the category of elements for the file: items, materials, weapons/armor, etc.
* ``{taxonomy}`` describes the Monster Hunter monster categories: "leviathan", "neopteron", etc.
* ``{lang}`` is a [ISO 639-1 language code](https://www.loc.gov/standards/iso639-2/php/English_list.php) for the language covered.


## Sources

Besides playing the game in Spanish, two main sources are used to compile the information: the Monster Hunter Wiki and the Kiranico Database. The information is, at present, compiled by hand.

